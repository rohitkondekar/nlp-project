import sys
import json
import math
import re
import random
from copy import copy, deepcopy

def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])

def find_skip_bigram(input_list,k):
  return  zip(input_list, input_list[k:])

def generateList(filename, bigram_dict, n):
    with open(filename,'r') as r_read:
        for line in r_read:
            words = line.strip().split()
            bi_list = find_ngrams(words,n)
            
            for l in bi_list:
                word = '+'.join(l)
                bigram_dict[word] = bigram_dict.get(word,0) + 1
    return bigram_dict

def cutoff(mydict,n):
    new_dict = dict()
    for key in mydict.keys():
        if not mydict[key]<n:
            new_dict[key] = mydict[key]
    mydict = new_dict
    return mydict

ngram_dict = dict()

folder = "Data/"
sarcastic_training_path = folder + "train_50/final_1_tweets_sarcasm_train.txt"
non_sarcastic_training_path = folder + "train_50/final_1_tweets_non_sarcasm_tokenized_en_training.txt"

sarcastic_training_pos_path = folder + "train_50/final_1_tweets_sarcasm_train.txt_pos"
non_sarcastic_training_pos_path = folder + "train_50/final_1_tweets_non_sarcasm_tokenized_en_training.txt_pos"


#sarcastic_training_path = folder + "final_1_tweets_sarcasm_train.txt_small"
#non_sarcastic_training_path = folder + "final_1_tweets_non_sarcasm_tokenized_en_training.txt_small"

#sarcastic_training_pos_path = folder + "final_1_tweets_sarcasm_train.txt_pos_small"
#non_sarcastic_training_pos_path = folder + "final_1_tweets_non_sarcasm_tokenized_en_training.txt_pos_small"



sarcastic_testing_path = folder+"test/final_1_tweets_sarcasm_test.txt"
sarcastic_testing_pos_path = folder+"test/final_1_tweets_sarcasm_test.txt_pos"

non_sarcastic_testing_path = folder+"test/final_1_tweets_non_sarcasm_tokenized_en_testing.txt"
non_sarcastic_testing_pos_path = folder+"test/final_1_tweets_non_sarcasm_tokenized_en_testing.txt_pos"

# Word 2 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,2)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,2)

# cutoff 50
cutoff(ngram_dict,50)

# Word 3 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,3)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,3)

# cutoff 25
cutoff(ngram_dict,25)

# Word 4 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,4)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,4)

# cutoff 15
cutoff(ngram_dict,15)

# POS tager ngram
ngram_pos_dict = dict()

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,3)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,3)

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,4)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,4)

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,5)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,5)

# cutoff 50
cutoff(ngram_pos_dict,25)


# Classification task
def readlines(fileName):
    with open(fileName,'r') as r_read:
        return r_read.readlines()

#Read data - generate feature set
#print('Here')

training_labels = []
testing_features = []
sarc = 0

tweetList = readlines(sarcastic_training_path)
postList = readlines(sarcastic_training_pos_path)

sarc = len(tweetList)
tweetList+=readlines(non_sarcastic_training_path)
postList+=readlines(non_sarcastic_training_pos_path)

training_labels = [1]*sarc+[0]*(len(tweetList)-sarc)

#print('Here2')
#shuffle lists
combined = zip(tweetList,postList,training_labels)
random.shuffle(combined)
tweetList[:], postList[:], training_labels[:] = zip(*combined)


# Generate Training Feature set

def generateFeature(line,postLine):
    ng = dict()
    np = dict()
    for j in range(2,5):
        gram_n = find_ngrams(str(line).strip().split(),j)
        for g in gram_n:
            ng['+'.join(g)] = ng.get('+'.join(g),0)+1
        
        pos_gram_n = find_ngrams(str(postLine).strip().split(),j+1)
        for p in pos_gram_n:
            np['+'.join(p)] = np.get('+'.join(p),0)+1
    
    return (ng,np)

#print('Here3')
training_features = []

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())
#training_features = numpy.zeros((len(tweetList),ln),dtype=numpy.int)
#training_features = [[0 for x in range(len(tweetList))] for x in range(ln)]
#print(ln)
#for i in range(0, len(tweetList)):
#    emptyArray = []
#    training_features.append(emptyArray)
#    for j in range(0, ln):
#        training_features[i].append(0)


ngram_keys = dict()
ngram_pos_keys = dict()

index = 0
for key in ngram_dict.keys():
    ngram_keys[key] = index
    index+=1

index = 0
for key in ngram_pos_dict.keys():
    ngram_pos_keys[key] = index
    index+=1


#rev_ngram_keys = dict()
#rev_ngram_pos_keys = dict()

#index = 0
#for key in ngram_dict.keys():
#    rev_ngram_keys[index] = key
#    index+=1

#index = 0
#for key in ngram_pos_dict.keys():
#    rev_ngram_pos_keys[index] = key
#    index+=1

#print('Here4')
lenNgramKeys = len(ngram_keys)
w_file = open('training_feature_file.txt','w')
for i in range(0,len(tweetList)):
    (ng,np) = generateFeature(tweetList[i],postList[i])
    if (i%10000 == 0):
        print(i)
#    w_file.write(str(training_labels[i]))
    line = str(training_labels[i])
    for j in range(0, lenNgramKeys+len(ngram_pos_keys)):
        
    for key in ng:
        if key in ngram_dict:
            line += " "+str(ngram_keys[key])+" "+str(ng[key])
#            w_file.write(str(ngram_keys[key])+" "+str(ng[key])+" ")
#            training_features[i][ngram_keys[key]] = ng[key]

    for key in np:
        if key in ngram_pos_dict:
             line += " "+str(lenNgramKeys + ngram_pos_keys[key])+" "+str(np[key])
#             w_file.write(str(lenNgramKeys + ngram_pos_keys[key])+" "+str(np[key])+" ")
    w_file.write(line+"\n")
#            training_features[i][len(ngram_keys)+ngram_pos_keys[key]] = np[key]
w_file.close()

# Get test data
w_tfile = open('testing_feature_file.txt','w')
test_tweetList = readlines(sarcastic_testing_path)
test_postList = readlines(sarcastic_testing_pos_path)
ct = len(test_tweetList)

test_tweetList+=readlines(non_sarcastic_testing_path)
test_postList+=readlines(non_sarcastic_testing_pos_path)
test_label = [1]*ct+[0]*(len(test_tweetList)-ct)


#shuffle lists
combined = zip(test_tweetList,test_postList,test_label)
random.shuffle(combined)
test_tweetList[:], test_postList[:], test_label[:] = zip(*combined)

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())

for i in range(0,len(test_tweetList)):
    (ng,np) = generateFeature(test_tweetList[i],test_postList[i])
    line = str(test_label[i])
#    line = ""
    for key in ng:
        if key in ngram_dict:
            line += " "+str(ngram_keys[key])+" "+str(ng[key])
#            line += str(ngram_keys[key])+" "+str(ng[key])+" "
#            testing_features[i][ngram_keys[key]] = ng[key]
    for key in np:
        if key in ngram_pos_dict:
            line += " "+str(lenNgramKeys+ngram_pos_keys[key])+" "+str(np[key])
#            line += str(lenNgramKeys+ngram_pos_keys[key])+" "+str(np[key])+" "
    w_tfile.write(line.rstrip()+"\n")
#            testing_features[i][len(ngram_keys)+ngram_pos_keys[key]] = np[key]
w_tfile.close()
#print('Done')
