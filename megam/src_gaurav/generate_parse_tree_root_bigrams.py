import sys
import re

def addToDict(word, dict_type):
    if word in dict_type:
        dict_type[word] += 1
    else:
        dict_type[word] = 1
    return dict_type

def cutoff(mydict,n):
    new_dict = dict()
    for key in mydict.keys():
        if mydict[key]>=n:
            new_dict[key] = mydict[key]
    mydict = new_dict
    return mydict

folder = "/home/gaurav/WinDown/NLP/"
#train1 = open(folder + "train_25/train1.txt)"
#train2 = open(folder + "training_feature_file_senti_initial_terminal.txt")
#trainR = open(folder + "training_feature_file_ngrams_pos_senti_initial_terminal_large.txt", 'w')
train2 = open(folder + "train_25/train1_predict.txt")
trainR = open(folder + "megam_files/traintest/training_feature_file_parse_root_bigram_raw.txt", 'w')
trainR2 = open(folder + "megam_files/traintest/training_feature_file_parse_root_bigram.txt", 'w')
#test1 = open(folder + "test/test1.txt")
test2 = open(folder + "test/test1_predict.txt")
testR = open(folder + "megam_files/traintest/testing_feature_file_parse_root_bigram_raw.txt", 'w')
testR2 = open(folder + "megam_files/traintest/testing_feature_file_parse_root_bigram.txt", 'w')
#test2 = open(folder + "testing_feature_file_senti_initial_terminal.txt")
#testR = open(folder + "testing_feature_file_ngrams_pos_senti_initial_terminal_large.txt", 'w')

lineC = train2.readline().strip()
lineN = train2.readline().strip()
dict_bigrams = {}

while lineC != '':
    rootList = []
    wordList = []
    while lineC != '':
        wordArray = re.split(r'\t+', lineC.strip())
        wordList.append(wordArray[1])
        rootList.append(wordArray[6])
        lineC = lineN
        lineN = train2.readline().strip()
    lineR = ""
    for i in range (0, len(wordList)):
#        print(wordList[i])
        part2 = '' 
        if int(rootList[i])>0: 
            part2 = wordList[int(rootList[i])-1]
        lineR += wordList[i] + "_*&##&*_" + part2 + " " 
        dict_bigrams = addToDict(wordList[i] + "_*&##&*_" + part2, dict_bigrams)
    trainR.write(lineR.strip() + "\n")
    lineC = lineN
    lineN = train2.readline().strip()
train2.close()
trainR.close()


#dict_bigrams = cutoff(dict_bigrams, 3)

train3 = open(folder + "megam_files/traintest/training_feature_file_parse_root_bigram_raw.txt")
lineC = train3.readline().strip()

while lineC != '':
    wordArray = lineC.strip().split()
    lineN = ''
    for word in wordArray:
        if word in dict_bigrams:
            lineN += word + " "+ str(dict_bigrams[word]) + " "
    trainR2.write(lineN.strip()+"\n")
    lineC = train3.readline().strip()

train3.close()
trainR2.close()


lineC = test2.readline().strip()
lineN = test2.readline().strip()

while lineC != '':
    rootList = []
    wordList = []
    while lineC != '':
        wordArray = re.split(r'\t+', lineC.strip())
        wordList.append(wordArray[1])
        rootList.append(wordArray[6])
        lineC = lineN
        lineN = test2.readline().strip()
    lineR = ""
    for i in range (0, len(wordList)):
#        print(wordList[i])
        part2 = ''
        if int(rootList[i])>0:
            part2 = wordList[int(rootList[i])-1]
        lineR += wordList[i] + "_*&##&*_" + part2 + " "
    testR.write(lineR.strip() + "\n")
    lineC = lineN
    lineN = test2.readline().strip()
test2.close()
testR.close()

test3 = open(folder + "megam_files/traintest/testing_feature_file_parse_root_bigram_raw.txt")
lineC = test3.readline().strip()

while lineC != '':
    wordArray = lineC.strip().split()
    lineN = ''
    for word in wordArray:
        if word in dict_bigrams:
            lineN += word + " "+ str(dict_bigrams[word]) + " "
    testR2.write(lineN.strip()+"\n")
    lineC = test3.readline().strip()

test3.close()
testR2.close()
