#!/bin/bash

## Compile 
javac -d bin/ -classpath .:libs/* -Xlint `find src | grep java$`

## Uses 1G of memory and specialized garbage collector for parallel programs. 

java -Xms1g -Xmx1g -XX:+UseTLAB -XX:+UseConcMarkSweepGC -cp .:bin/:libs/* main.RAEBuilder \
-DataDir data/sarcasm \
-MaxIterations 50 \
-ModelFile data/sarcasm/tunedTheta.rae \
-ClassifierFile data/sarcasm/Softmax.clf \
-NumCores 10 \
-TrainModel True \
-ProbabilitiesOutputFile data/sarcasm/prob.out \
-TreeDumpDir data/sarcasm/trees

#java -Xms1g -Xmx1g -XX:+UseTLAB -XX:+UseConcMarkSweepGC -cp .:bin/:libs/* main.RAEBuilder \
#-DataDir data/
#-MaxIterations 80
#-ModelFile data/mov/tunedTheta.rae
#-ClassifierFile data/mov/Softmax.clf
#-NumCores 2
#-TrainModel False
#-ProbabilitiesOutputFile data/tiny/prob.out
