make
if [ $? -eq 0 ]; then
	echo "Running: $1 $2 $3 $4"
	#cp $1train.data ./train.data
	#cp $1test.data ./test.data
	./simple_train $1 $2 $3 $4 > logs/$2train.log
	./simple_test $1 $2 $3 > logs/$2test.log
	#rm test.data
	#cp $1small_test.data ./test.data
	./simple_test $1small_ $2 $3 > logs/$2small_test.log
	#rm train.data
	#rm test.data
else
    echo "FAIL"
fi
