
# coding: utf-8

# In[2]:

import os
import sys
import glob
import re


# In[3]:

link = re.compile(r'http|www|https')
rt = re.compile(r'^RT')


# In[23]:
def filterTweets(filename,labelFile):
    tweets = set()
    with open(filename,'r') as r_file:
        with open(labelFile,'r') as r2_file:

            tweets = r_file.readlines()
            labels = r2_file.readlines()

            for index in range(0,len(tweets)):
                tweet = tweets[index]

                #ignore tweets with link
                if link.search(tweet) is not None:
                    continue
                #ignore retweets
                if rt.search(tweet) is not None:
                    continue
                #Remove non-latin characters
                tweet = re.sub(ur'[^#\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', tweet)
                #Remove words starting with @
                tweet = re.sub('@[^ ]*','',tweet)
                #Remove Special Characters
                tweet = re.sub('[/\n:;]','',tweet)
                #lowercase everything
                tweet = tweet.lower()

                if len(tweet.split())<=2:
                    continue

                print labels[index].strip()+"\t"+tweet.strip()

if __name__ == '__main__':
    filterTweets(sys.argv[1],sys.argv[2])      
