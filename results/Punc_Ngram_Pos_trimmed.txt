Punc_Ngram_Pos

running SVM training
             precision    recall  f1-score   support

non_sarcasm       0.93      0.90      0.91     22414
    sarcasm       0.56      0.65      0.60      4483

avg / total       0.87      0.86      0.86     26897

             precision    recall  f1-score   support

non_sarcasm       0.89      0.98      0.93     22414
    sarcasm       0.78      0.40      0.53      4483

avg / total       0.87      0.88      0.87     26897

             precision    recall  f1-score   support

non_sarcasm       0.92      0.96      0.94     22414
    sarcasm       0.73      0.57      0.64      4483

avg / total       0.89      0.89      0.89     26897

             precision    recall  f1-score   support

non_sarcasm       0.93      0.93      0.93     22414
    sarcasm       0.63      0.64      0.63      4483

avg / total       0.88      0.88      0.88     26897

             precision    recall  f1-score   support

non_sarcasm       0.93      0.88      0.90     22414
    sarcasm       0.52      0.65      0.57      4483

avg / total       0.86      0.84      0.85     26897

             precision    recall  f1-score   support

non_sarcasm       0.92      0.86      0.89     22414
    sarcasm       0.48      0.64      0.55      4483

avg / total       0.85      0.82      0.83     26897