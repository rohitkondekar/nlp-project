
# coding: utf-8

# In[1]:

import sys
import json
import math
import re
import numpy
import random
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from copy import copy, deepcopy
from sklearn import metrics
from scipy import sparse

# Generat N grams and skip grams

# In[2]:

def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])


# In[3]:

def find_skip_bigram(input_list,k):
  return  zip(input_list, input_list[k:])


# In[4]:

def generateList(filename, bigram_dict, n, skip=False):
    with open(filename,'r') as r_read:
        for line in r_read:
            words = line.strip().split()

            if skip==False:
                bi_list = find_ngrams(words,n)
            else:
                bi_list = find_skip_bigram(words,3)
            
            for l in bi_list:
                word = '+'.join(l)
                bigram_dict[word] = bigram_dict.get(word,0) + 1
    return bigram_dict


# In[5]:

def cutoff(mydict,n):
    for key in mydict.keys():
        if mydict[key]<n:
            del mydict[key]
    return mydict


# In[6]:

ngram_dict = dict()

folder = "/home/mahesh_t/rohit_work/nlp-project/data/"
sarcastic_training_path = folder + "25_75_training/sarcasm/final_1_tweets_sarcasm_train.txt"
non_sarcastic_training_path = folder + "25_75_training/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_training.txt"

sarcastic_training_pos_path = sarcastic_training_path #folder + "25_75_training/sarcasm/final_1_tweets_sarcasm_train.txt_pos"
non_sarcastic_training_pos_path = non_sarcastic_training_path #folder + "25_75_training/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_training.txt_pos"

sarcastic_testing_path = folder+"testing/sarcasm/final_1_tweets_sarcasm_test.txt"
non_sarcastic_testing_path = folder+"testing/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_testing.txt"

sarcastic_testing_pos_path = sarcastic_testing_path #folder+"testing/sarcasm/final_1_tweets_sarcasm_test.txt_pos"
non_sarcastic_testing_pos_path = non_sarcastic_testing_path #folder+"testing/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_testing.txt_pos"

# Unigram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,1)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,1)

# Word 2 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,2)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,2)

# Word 3 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,3)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,3)

# cutoff 15
cutoff(ngram_dict,3)

print len(ngram_dict.keys())


# POS tager ngram

# In[7]:

# skipgrams are left named as pos dict

ngram_pos_dict = dict()

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,0,True)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,0,True)

# cutoff 10 for skipgrams
cutoff(ngram_pos_dict,10)
print len(ngram_pos_dict.keys())


# Classification task

# In[8]:

def readlines(fileName):
    with open(fileName,'r') as r_read:
        return r_read.readlines()


# In[9]:

#Read data - generate feature set

training_labels = []
testing_features = []
sarc = 0

tweetList = readlines(sarcastic_training_path)
postList = readlines(sarcastic_training_pos_path)

sarc = len(tweetList)
tweetList+=readlines(non_sarcastic_training_path)
postList+=readlines(non_sarcastic_training_pos_path)

training_labels = [1]*sarc+[0]*(len(tweetList)-sarc)

print len(tweetList)
print len(postList)


# In[10]:

#shuffel lists
combined = zip(tweetList,postList,training_labels)
random.shuffle(combined)
tweetList[:], postList[:], training_labels[:] = zip(*combined)


# Generate Training Feature set

# In[11]:

def generateFeature(line,postLine):
    ng = dict()
    np = dict()
    for j in range(1,4):
        gram_n = find_ngrams(str(line).strip().split(),j)
        for g in gram_n:
            ng['+'.join(g)] = ng.get('+'.join(g),0)+1

    pos_gram_n = find_skip_bigram(str(postLine).strip().split(),3)
    for p in pos_gram_n:
        np['+'.join(p)] = np.get('+'.join(p),0)+1

    return (ng,np)


# In[12]:

training_features = []

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())
training_features = sparse.lil_matrix((len(tweetList),ln), dtype=numpy.int)


# In[13]:

ngram_keys = dict()
ngram_pos_keys = dict()

index = 0
for key in ngram_dict.keys():
    ngram_keys[key] = index
    index+=1

index = 0
for key in ngram_pos_dict.keys():
    ngram_pos_keys[key] = index
    index+=1


# In[14]:

for i in range(0,len(tweetList)):
    (ng,np) = generateFeature(tweetList[i],postList[i])
    
    for key in ng:
        if key in ngram_dict:
            training_features[i,ngram_keys[key]] = ng[key]
            
    for key in np:
        if key in ngram_pos_dict:
            training_features[i,len(ngram_keys)+ngram_pos_keys[key]] = np[key]

print ngram_pos_keys.keys()[1:10]
# Run SVM

print "running SVM training"
# In[ ]:
clf = LinearSVC()
clf.fit(training_features, training_labels)  


# Get test data

# In[23]:

testing_features = []

test_tweetList = readlines(sarcastic_testing_path)
test_postList = readlines(sarcastic_testing_pos_path)
ct = len(test_tweetList)

test_tweetList+=readlines(non_sarcastic_testing_path)
test_postList+=readlines(non_sarcastic_testing_pos_path)

test_label = [1]*ct+[0]*(len(test_tweetList)-ct)


# In[24]:

testing_features = []

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())
testing_features = sparse.lil_matrix( (len(test_tweetList),ln), dtype=numpy.int )


# In[25]:

for i in range(0,len(test_tweetList)):
    (ng,np) = generateFeature(test_tweetList[i],test_postList[i])
    
    for key in ng:
        if key in ngram_dict:
            testing_features[i,ngram_keys[key]] = ng[key]
            
    for key in np:
        if key in ngram_pos_dict:
            testing_features[i,len(ngram_keys)+ngram_pos_keys[key]] = np[key]

# In[27]:

for c in [0.001,.01,.1,10,100,1000]:
    result_class = clf.predict(testing_features)
    print(metrics.classification_report(test_label, result_class, target_names=['non_sarcasm','sarcasm']))

    clf = LinearSVC(C=c)
    clf.fit(training_features, training_labels)  

