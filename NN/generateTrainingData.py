__author__ = 'Ankit'

import sys
import re
import operator
import random
import itertools
from scipy import array, zeros, signal
from scipy.fftpack import fft, ifft, convolve
import math
import numpy
from collections import defaultdict


def getWordVectors(vectorFile, emoticonsWordMap):
    try:
        inFile = open(vectorFile, "r", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + vectorFile + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    wordVectors = {}
    for line in inFile:
        line = line.strip()
        tokens = line.split(' ')

        word = tokens[0]
        tokens = tokens[1:]
        vector = [float(x) for x in tokens]
        wordVectors[word] = vector

    for emoticon in emoticonsWordMap:
        if emoticon not in wordVectors and emoticonsWordMap[emoticon] in wordVectors:
        	wordVectors[emoticon] = wordVectors[emoticonsWordMap[emoticon]]

    inFile.close()
    return wordVectors


def getEmticonMeanings(emoticonsFile):
    emoticonsWordMap = {}
    #try:
    inFile = open(emoticonsFile, "r", encoding='iso-8859-15')
    #except:
    #    sys.stderr.write("Can't open emoticons file for reading.\n" + emoticonsFile)
    #    raise IOError("Can't open the input text file.")

    for line in inFile:
        emoticons = line.strip().split(",")
        meaning = emoticons[0].strip().lower()
        emoticons = emoticons[1:]
        for emoticon in emoticons:
            emoticonsWordMap[emoticon.strip()] = meaning

    inFile.close()
    return emoticonsWordMap


def sanitizeSample(sample):
    # TODO: Match Word2Vec
    sample = sample.strip().lower()
    return sample


def normalizeVector(v):
	sum = 0
	for item in v:
		sum += item*item
	sum = math.sqrt(sum)
	return divideVectorByScalar(v, sum)


def divideVectorByScalar(v, s):
    if s != 0:
        for i in range(0, len(v)):
            v[i] /= s
    return v


def concatenateRepresentations(v1, v2):
	#for element in v2:
	#	v1.append(element)
	return normalizeVector(numpy.append(v1,v2))


def getAdditionRepresentation(words, wordVectors):
    representation = [0] * len(wordVectors["the"])
    count =0
    for word in words:
        if word in wordVectors:
            vec = wordVectors[word]
            count += 1
            for i in range(0, len(representation)):
                representation[i] += vec[i]

    return divideVectorByScalar(representation, count)


def convolve1(f, g):
	sum = [0] * len(f)
	for i in range(0, len(f)):
		for j in range(0, len(g)):
			sum[i] += f[i] * g[(i+j)%len(f)]
	return sum
	

def convolve(f, g):
	F = fft(f)
	G = fft(g)
	# multiply entry-wise
	C = F * G
	# transfer C to time domain
	c = ifft(C).real
	return c
	

def getConvolutionalRepresentation(words, wordVectors):
    count =0
    representation =  [0] * len(wordVectors["the"])

    for i in range(0,len(words)):
    	count += 1
    	if words[i] in wordVectors:
    		representation = wordVectors[words[i]]
    		break

    avgCount = 0
    for i in range(count,len(words)):
    	if words[i] in wordVectors:
    		avgCount += 1
    		representation = convolve(representation, normalizeVector(wordVectors[words[i]]))
    		representation = normalizeVector(representation)

    return divideVectorByScalar(representation, avgCount)


def getConvolutionalRepresentation_additionBiGram(words, wordVectors):
    wordList = []
    for w in words:
    	if w in wordVectors:
    		wordList.append(w)

    count =0
    representation = [0] * len(wordVectors["the"])
    for i in range(0,len(wordList)-1):
    	count += 1
    	representation += convolve(wordVectors[wordList[i]], wordVectors[wordList[i+1]])

    representation = normalizeVector(representation)
    return divideVectorByScalar(representation, count)


def generateInputVector(sample, wordVectors, mode="1"):
    sample = sanitizeSample(sample)
    words = re.split("[ \t\n]", sample)
    if mode == "1" or mode == "4" or mode == "5":
        simpleAddtionRepresentation = getAdditionRepresentation(words, wordVectors)
        if mode == "1":
            return simpleAddtionRepresentation
    if mode == "2" or mode == "4":
        convolutionalRepresentation = getConvolutionalRepresentation(words, wordVectors)
        if mode == "2":
            return convolutionalRepresentation
    if mode == "3" or mode == "5":
    	convolutionalRepresentation = getConvolutionalRepresentation_additionBiGram(words, wordVectors)
    	if mode  == "3":
            return convolutionalRepresentation
    if mode == "4" or mode == "5":
        representation = concatenateRepresentations(convolutionalRepresentation, simpleAddtionRepresentation)
        return representation
    # TODO: Use Parser and Build Hierarchical Representation of the Sentence


def generateDataFile(wordVectors, inputFileName, inputFileLabel, outputFileName, vecmode="1", mode="w"):
    try:
        inFile = open(inputFileName, "r", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + inputFileName + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    try:
        outFile = open(outputFileName, mode, encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + outputFileName + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    for sample in inFile:
        nnInput = generateInputVector(sample, wordVectors, vecmode)
        outFile.write(" ".join(map(str, nnInput)))
        outFile.write("\n")
        outFile.write(inputFileLabel)
        outFile.write("\n")

    outFile.close()
    inFile.close()


def shuffleSamples(filename):
    tuples = []
    with open(filename, encoding='iso-8859-15') as f:
        while True:
            line1 = f.readline()
            line2 = f.readline()
            if not line2:
                break
            tuples.append((line1, line2))

    numSamples = len(tuples)
    inputSize = len(tuples[0][0].split(' '))

    random.shuffle(tuples)
    try:
        outFile = open(filename, "w", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + filename + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    outFile.write("{} {} 1\n".format(numSamples, inputSize))
    for sample in tuples:
        outFile.write(sample[0])
        outFile.write(sample[1])

    outFile.close()

def subSampleTestFile(filename, smallFile, ratio):
    tuples = []
    non_sarcasm_tuples = []
    sarcasm_tuples = []
    count = [0, 0]
    with open(filename, encoding='iso-8859-15') as f:
        f.readline()
        while True:
            line1 = f.readline()
            line2 = f.readline()
            if not line2:
                break
            if line2.strip() == "-1":
            	count[0] += 1
            	non_sarcasm_tuples.append((line1, line2))
            else:
            	count[1] += 1
            	sarcasm_tuples.append((line1, line2))

    tuples = non_sarcasm_tuples[0:(len(sarcasm_tuples)*ratio)]
    for tuple in sarcasm_tuples:
        tuples.append(tuple)
    numSamples = len(tuples)
    inputSize = len(tuples[0][0].split(' '))
    random.shuffle(tuples)
    try:
        outFile = open(smallFile, "w", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + smallFile + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    outFile.write("{} {} 1\n".format(numSamples, inputSize))
    for sample in tuples:
    	outFile.write(sample[0])
    	outFile.write(sample[1])
    outFile.close()


def getParseTreeRepresentation(r, dependencyList, phraseVectorList, wordIds):
	if r in phraseVectorList:
		return phraseVectorList[r]
	else:
		representation = [0] * len(wordVectors["the"])
		if r in dependencyList:
			#print("(")
			for child in dependencyList[r]:
				#print(child)
				childVector = getParseTreeRepresentation(child, dependencyList, phraseVectorList, wordIds)
				for i in range(0, len(representation)):
					representation[i] += childVector[i]
			representation = divideVectorByScalar(representation, len(dependencyList[r]))
			#print(")")

		if wordIds[r] in wordVectors:
			vec = wordVectors[wordIds[r]]
			for i in range(0, len(representation)):
				representation[i] += vec[i]
			representation = divideVectorByScalar(representation, 2)
		phraseVectorList[r] = representation
		return representation


def readParseTreeAndConstructVectors(wordVectors, inputFileName, inputFileLabel, outputFileName, mode="w"):
    try:
        inFile = open(inputFileName, "r", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + inputFileName + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    try:
        outFile = open(outputFileName, mode, encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + outputFileName + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    wordList = []
    wordIds = {}
    roots = []
    phraseVectorList = {}
    dependencyList = {}
    for line in inFile:
    	temp = line.strip()
    	if len(temp) == 0:
    		# Create Tree Structure:
    		for tuple in wordList:
    			if tuple[1] == 0:
    				roots.append(tuple[0]) 
    			elif not tuple[1] == -1:
    				if wordList[tuple[1]-1][0] in dependencyList:
    					dependencyList[wordList[tuple[1]-1][0]].add(tuple[0])
    				else:
    					dependencyList[wordList[tuple[1]-1][0]] = set()
    					dependencyList[wordList[tuple[1]-1][0]].add(tuple[0])

    		# Generate Vector Representations from Leaf to Roots.
    		representation = [0] * len(wordVectors["the"])
    		for r in roots:
    			rootVector = getParseTreeRepresentation(r, dependencyList, phraseVectorList, wordIds)
    			for i in range(0, len(representation)):
    				representation[i] += rootVector[i]

    		representation = divideVectorByScalar(representation, len(roots))
    		representation = normalizeVector(representation)

    		outFile.write(" ".join(map(str, representation)))
    		outFile.write("\n")
    		outFile.write(inputFileLabel)
    		outFile.write("\n")

    		wordList = []
    		roots = []
    		wordIds = {}
    		phraseVectorList = {}
    		dependencyList = defaultdict(set)
    	else:
    		temp = temp.split('\t')
    		wordList.append((int(temp[0]), int(temp[6])))
    		wordIds[int(temp[0])] = temp[1].strip()

    outFile.close()
    inFile.close()


def shuffleSamples(filename):
    tuples = []
    with open(filename, encoding='iso-8859-15') as f:
        while True:
            line1 = f.readline()
            line2 = f.readline()
            if not line2:
                break
            tuples.append((line1, line2))

    numSamples = len(tuples)
    inputSize = len(tuples[0][0].split(' '))

    random.shuffle(tuples)
    try:
        outFile = open(filename, "w", encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open file `" + filename + "' for reading.\n")
        raise IOError("Can't open the input text file.")

    outFile.write("{} {} 1\n".format(numSamples, inputSize))
    for sample in tuples:
        outFile.write(sample[0])
        outFile.write(sample[1])

    outFile.close()


if __name__ == "__main__":
    argList = sys.argv[1:]
    if len(argList) < 1:
        print("Usage: python3 processing.py path/to/word/vectors outputPrefix")
        exit(1)

    if argList[3] == "parseTree":
    	sarcasm_train = argList[2] + "sarcasm_train.txt.predict"
    	sarcasm_test = argList[2] + "sarcasm_test.txt.predict"
    	non_sarcasm_train = argList[2] + "non_sarcasm_train.txt.predict"
    	non_sarcasm_test = argList[2] + "non_sarcasm_test.txt.predict"
    else:
    	sarcasm_train = argList[2] + "sarcasm_train.txt"
    	sarcasm_test = argList[2] + "sarcasm_test.txt"
    	non_sarcasm_train = argList[2] + "non_sarcasm_train.txt"
    	non_sarcasm_test = argList[2] + "non_sarcasm_test.txt"
    sarcasm = "1"
    non_sarcasm = "-1"
    vecmode = argList[3]

    prefix = "trainTestData"
    if vecmode == "parseTree":
        prefix =  "trainTestDataparseTree"
    
    trainFile = prefix + "/" + argList[1] + "train.data"
    testFile =  prefix + "/" + argList[1] + "test.data"
    smalltestFile =  prefix + "/" + argList[1] + "small_test.data"

    # SETUP
    emoticonsWordMap = getEmticonMeanings("../processedData/emoticons.csv")
    print("Emoticons Loaded...")
    wordVectors = getWordVectors(argList[0], emoticonsWordMap)
    print("Word Vectors Loaded...")

    # INPUT FILES
    if not argList[3] == "parseTree":
    	generateDataFile(wordVectors, sarcasm_train, sarcasm, trainFile, vecmode)
    else:
    	readParseTreeAndConstructVectors(wordVectors, sarcasm_train, sarcasm, trainFile)
    print("Training Samples Sarcasm Loaded...")

    if not argList[3] == "parseTree":
    	generateDataFile(wordVectors, non_sarcasm_train, non_sarcasm, trainFile, vecmode, "a")
    else:
    	readParseTreeAndConstructVectors(wordVectors, non_sarcasm_train, non_sarcasm, trainFile, "a")
    print("Training Samples Non Sarcasm Loaded...")

    if not argList[3] == "parseTree":
    	generateDataFile(wordVectors, sarcasm_test, sarcasm, testFile, vecmode)
    else:
    	readParseTreeAndConstructVectors(wordVectors, sarcasm_test, sarcasm, testFile)    
    print("Test Samples Sarcasm Loaded...")

    if not argList[3] == "parseTree":
    	generateDataFile(wordVectors, non_sarcasm_test, non_sarcasm, testFile, vecmode, "a")
    else:
    	readParseTreeAndConstructVectors(wordVectors, non_sarcasm_test, non_sarcasm, testFile, "a")    
    print("Test Samples Non Sarcasm Loaded...")

    # INPUT FILES POLISH
    shuffleSamples(trainFile)
    print("Training Samples Shuffled...")
    shuffleSamples(testFile)
    print("Test Samples Shuffled...")
    subSampleTestFile(testFile, smalltestFile, 5)
    print("Test Sub-Sampled...")


# Temporary Script for Tweet Extractions from Raw Files.
def extractTweetsFromDataSet():
    try:
        inFile  =  open ("source.csv", "r", encoding='iso-8859-15')
    except:
        sys.stderr.write ( "Can't open source file for reading.\n")
        raise IOError("Can't open the input text file.")

    try:
        outFile  =  open ("tweets.txt", "w", encoding='iso-8859-15')
    except:
        sys.stderr.write ( "Can't open target file for reading.\n")
        raise IOError("Can't open the input text file.")

    for line in inFile:
        # Some Extraction Login according to source format.
        #tweet = line.strip()
        words = line.strip().split(",")
        tweet = words[5].strip()
        if tweet[:1] == "\"":
            tweet = tweet[1:]
        if tweet[-1:] == "\"":
            tweet = tweet[:-1]
        outFile.write(tweet.strip())
        outFile.write("\n")

    inFile.close()
    outFile.close()
    return
