
# coding: utf-8

# In[2]:

import os
import sys
import glob
import re


# In[3]:

link = re.compile(r'http|www|https')
rt = re.compile(r'^RT')


# In[23]:
tweets = set()
with open('/Users/rohitkondekar/Dropbox/NLP Project/outputs/non sarcastic/non_sarcastic_1_tweets.txt','w') as w_file: 
    for file in glob.glob("/Users/rohitkondekar/Dropbox/NLP Project/outputs/non sarcastic/*"):
        with open(file,'r') as r_file:
            for tweet in r_file:
                #ignore tweets with link
                if link.search(tweet) is not None:
                    continue
                #ignore retweets
                if rt.search(tweet) is not None:
                    continue
                #Remove non-latin characters
                tweet = re.sub(ur'[^#\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', tweet)
                #Remove words starting with @
                tweet = re.sub('@[^ ]*','',tweet)
                #Remove Special Characters
                tweet = re.sub('[/\n:;]','',tweet)
                #lowercase everything
                tweet = tweet.lower()

                if len(tweet.split())<=2:
                    continue

                w_file.write(tweet.strip()+"\n")
            


# In[ ]:



