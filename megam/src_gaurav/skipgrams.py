import sys
import json
import math
import re
import random
from copy import copy, deepcopy

def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])

def find_skip_bigram(input_list,k):
  return  zip(input_list, input_list[k:])

def generateList(filename, bigram_dict, n, skip=False):
    with open(filename,'r') as r_read:
        for line in r_read:
            words = line.strip().split()

            if skip==False:
                bi_list = find_ngrams(words,n)
            else:
                bi_list = find_skip_bigram(words,3)

            for l in bi_list:
                word = '+'.join(l)
                bigram_dict[word] = bigram_dict.get(word,0) + 1
    return bigram_dict

def cutoff(mydict,n):
    new_dict = dict()
    for key in mydict.keys():
        if mydict[key]>=n:
            new_dict[key] = mydict[key]
    mydict = new_dict
    return mydict

ngram_dict = dict()
ngram_pos_dict = dict()

folder = "data/"
sarcastic_training_path = folder + "train_25/final_1_tweets_sarcasm_train.txt"
non_sarcastic_training_path = folder + "train_25/final_1_tweets_non_sarcasm_tokenized_en_training.txt"

sarcastic_training_pos_path = folder + "train_25/final_1_tweets_sarcasm_train.txt_pos.txt"
non_sarcastic_training_pos_path = folder + "train_25/final_1_tweets_non_sarcasm_tokenized_en_training.txt_pos.txt"

sarcastic_testing_path = folder+"test/final_1_tweets_sarcasm_test.txt"
sarcastic_testing_pos_path = folder+"test/final_1_tweets_sarcasm_test.txt_pos.txt"

non_sarcastic_testing_path = folder+"test/final_1_tweets_non_sarcasm_tokenized_en_testing.txt"
non_sarcastic_testing_pos_path = folder+"test/final_1_tweets_non_sarcasm_tokenized_en_testing.txt_pos.txt"

# Megam train and test data paths
w_file_path = '/home/gaurav/WinDown/NLP/megam_files/traintest/training_feature_file_skipgrams.txt'
w_tfile_path = '/home/gaurav/WinDown/NLP/megam_files/traintest/testing_feature_file_skipgrams.txt'

# ngrams
#ngram_dict = generateList(sarcastic_training_path,ngram_dict,1)
#ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,1)
#ngram_dict = generateList(sarcastic_training_path,ngram_dict,2)
#ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,2)

#print(len(ngram_dict))
#ngram_dict = cutoff(ngram_dict, 50)
#print(len(ngram_dict))

#ngram_dict = generateList(sarcastic_training_path,ngram_dict,3)
#ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,3)
#ngram_dict = cutoff(ngram_dict, 25)

ngram_dict = generateList(sarcastic_training_path,ngram_dict,0,True)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,0,True)
ngram_dict = cutoff(ngram_dict, 10)

# Classification task
def readlines(fileName):
    with open(fileName,'r') as r_read:
        return r_read.readlines()

#Read data - generate feature set
print('Here')

training_labels = []
testing_features = []
sarc = 0

tweetList = readlines(sarcastic_training_path)
postList = readlines(sarcastic_training_pos_path)

sarc = len(tweetList)
tweetList+=readlines(non_sarcastic_training_path)
postList+=readlines(non_sarcastic_training_pos_path)

training_labels = [1]*sarc+[0]*(len(tweetList)-sarc)

#print('Here2')
#shuffle lists
combined = zip(tweetList,postList,training_labels)
random.shuffle(combined)
tweetList[:], postList[:], training_labels[:] = zip(*combined)

# Generate Training Feature set
def generateFeature(line,postLine):
    ng = dict()
    np = dict()
#    for j in range(1,4):
    gram_n = find_skip_bigram(str(postLine).strip().split(),3)
    for g in gram_n:
        ng['+'.join(g)] = ng.get('+'.join(g),0)+1

#        pos_gram_n = find_ngrams(str(postLine).strip().split(),j+1)
#        for p in pos_gram_n:
#            np['+'.join(p)] = np.get('+'.join(p),0)+1

    return (ng,np)

#print('Here3')

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())

ngram_keys = dict()
ngram_pos_keys = dict()

index = 0
for key in ngram_dict.keys():
    ngram_keys[key] = index
    index+=1

index = 0
for key in ngram_pos_dict.keys():
    ngram_pos_keys[key] = index
    index+=1

#print(len(ngram_keys)+len(ngram_pos_keys))

rev_ngram_keys = dict()
rev_ngram_pos_keys = dict()

index = 0
for key in ngram_dict.keys():
    rev_ngram_keys[index] = key
    index+=1

index = 0
for key in ngram_pos_dict.keys():
    rev_ngram_pos_keys[index] = key
    index+=1

#print('Here4')
lenNgramKeys = len(ngram_keys)
w_file = open(w_file_path,'w')
for i in range(0,len(tweetList)):
    (ng,np) = generateFeature(tweetList[i],postList[i])
    if (i%100 == 0):
        print(i)
    line = str(training_labels[i])
    for j in range(0, lenNgramKeys):
        key = rev_ngram_keys[j]
        if key in ng:
            line += " "+str(j)+" "+str(ng[key])
#    for j in range(lenNgramKeys, len(ngram_pos_keys)):
#        key = rev_ngram_pos_keys[j]
#        if key in np:
#             line += " "+str(j)+" "+str(np[key])
    w_file.write(line+"\n")
w_file.close()

# Get test data
w_tfile = open(w_tfile_path,'w')
test_tweetList = readlines(sarcastic_testing_path)
test_postList = readlines(sarcastic_testing_pos_path)
ct = len(test_tweetList)

test_tweetList+=readlines(non_sarcastic_testing_path)
test_postList+=readlines(non_sarcastic_testing_pos_path)
test_label = [1]*ct+[0]*(len(test_tweetList)-ct)

ln = len(ngram_dict.keys())+len(ngram_pos_dict.keys())

for i in range(0,len(test_tweetList)):
    (ng,np) = generateFeature(test_tweetList[i],test_postList[i])
    if (i%100 == 0):
        print(i)
    line = str(test_label[i])
    for j in range(0, lenNgramKeys):
        key = rev_ngram_keys[j]
        if key in ng:
            line += " "+str(j)+" "+str(ng[key])
#    for j in range(lenNgramKeys, len(ngram_pos_keys)):
#        key = rev_ngram_pos_keys[j]
#        if key in np:
#            line += " "+str(j)+" "+str(np[key])
    w_tfile.write(line.rstrip()+"\n")
w_tfile.close()

