__author__ = 'Ankit'

import sys
import re
import operator
import random
import itertools


def getNegPosFiles(file1, file2, mode="w"):
    try:
        negFile = open("data/tweets/tweets.neg", mode, encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open emoticons file for reading.\n")
        raise IOError("Can't open the input text file.")
    try:
        posFile = open("data/tweets/tweets.pos", mode, encoding='iso-8859-15')
    except:
        sys.stderr.write("Can't open emoticons file for reading.\n")
        raise IOError("Can't open the input text file.")

    with open(file1, encoding='iso-8859-15') as f1, open(file2, encoding='iso-8859-15') as f2:
        for sample in f1:
            sample = sample.strip()
            label = f2.readline().strip()
            if label == "0":
                negFile.write(sample + "\n")
            else:
                posFile.write(sample + "\n")

    negFile.close()
    posFile.close()


if __name__ == "__main__":
    argList = sys.argv[1:]

    getNegPosFiles("../../data/sentiment_data/train_set.txt", "../../data/sentiment_data/train_label.txt")
    getNegPosFiles("../../data/sentiment_data/test_set.txt", "../../data/sentiment_data/test_label.txt", "a")

