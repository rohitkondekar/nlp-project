/*
Fast Artificial Neural Network Library (fann)
Copyright (C) 2003-2012 Steffen Nissen (sn@leenissen.dk)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include<string.h>
#include "floatfann.h"

int main(int argc, char * argv[])
{
	fann_type *calc_out;
	int input_size = atoi(argv[3]);
	fann_type input[input_size];
	int label = 1;
	int realAns[2] = {0,0};
	int predictionAns[2] = {0,0};
	int correctPred[2] = {0,0};
	int i, total=0, totalCorrect=0;
	int cnt=0;
	char temp[200] = "models/";
	strcat(temp, argv[2]);
        
	struct fann *ann = fann_create_from_file(strcat(temp,"sarcasm.net"));

	FILE *ifp;
	char *mode = "r";
	ifp = fopen(strcat(argv[1],"test.data"), mode);

	if (ifp == NULL) {
	  fprintf(stderr, "Can't open input file test.data!\n");
	  exit(1);
	}

	  	fscanf(ifp, "%d", &label);
	  	fscanf(ifp, "%d", &label);
	  	fscanf(ifp, "%d", &label);
	
	while (!feof(ifp)) {
		for(i=0; i<input_size; i++) {
			fscanf(ifp, "%f", &input[i]);
			//printf("%f ", input[i]);
		}
		//printf("\n");
	  	fscanf(ifp, "%d", &label);
		calc_out = fann_run(ann, input);
		/*		
		printf("%d %f\n", label, calc_out[0]);
		
		cnt++;
		if(cnt==10) {
		exit(-1);
		break;
		}*/
		int k =0;
		if(label == -1) {
			k = 1;
		}
		realAns[k]++;
		float temp = calc_out[0];
		printf("%d %f\n", label, temp);
		int pred = 0;
		if(temp < 0)
			pred = 1;
		//printf("real:%d pred:%d %f\n\n", k, pred, temp);
		predictionAns[pred]++;
		total++;
		if(pred == k) {
			totalCorrect++;
			correctPred[pred]++;
		}
	}
	
	for(i=0; i<2; i++) {
		float recall = ((float)correctPred[i])/realAns[i];
		float precision = ((float)correctPred[i])/predictionAns[i];
		float fscore = 2*precision*recall/(precision+recall);
		printf("%d\tPrecision: %f\tRecall: %f\t Fscore: %f\n", i, precision, recall, fscore);
	}
	
	printf("Accurcy: %d of %d correct, %f\n", totalCorrect, total, ((float)totalCorrect)/total);

	fann_destroy(ann);
	return 0;
}
