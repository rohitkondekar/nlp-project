import sys
import json
import math
import re
import numpy
import random
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from copy import copy, deepcopy
from sklearn import metrics
from scipy import sparse
from sklearn.preprocessing import normalize

def find_ngrams(input_list, n):
  return zip(*[input_list[i:] for i in range(n)])

def generateList(filename, bigram_dict, n):
    with open(filename,'r') as r_read:
        for line in r_read:
            words = line.strip().split()
            bi_list = find_ngrams(words,n)
            
            for l in bi_list:
                word = '+'.join(l)
                bigram_dict[word] = bigram_dict.get(word,0) + 1
    return bigram_dict

def cutoff(mydict,n):
    for key in mydict.keys():
        if mydict[key]<n:
            del mydict[key]
    return mydict

puncs = ['!','?','\'','"','`']
puncs = sorted(set(puncs))

def getPuncFeatures(line):

    features = []
    for p in puncs:
        features.append(line.count(p))

    return features


ngram_dict = dict()

folder = "/home/mahesh_t/rohit_work/nlp-project/data/"
sarcastic_training_path = folder + "25_75_training/sarcasm/final_1_tweets_sarcasm_train.txt"
non_sarcastic_training_path = folder + "25_75_training/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_training.txt"

sarcastic_training_pos_path = folder + "25_75_training/sarcasm/final_1_tweets_sarcasm_train.txt_pos"
non_sarcastic_training_pos_path = folder + "25_75_training/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_training.txt_pos"

sarcastic_testing_path = folder+"testing/sarcasm/final_1_tweets_sarcasm_test.txt"
sarcastic_testing_pos_path = folder+"testing/sarcasm/final_1_tweets_sarcasm_test.txt_pos"

non_sarcastic_testing_path = folder+"testing/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_testing.txt"
non_sarcastic_testing_pos_path = folder+"testing/non_sarcasm/final_1_tweets_non_sarcasm_tokenized_en_testing.txt_pos"

# Unigram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,1)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,1)

# Word 2 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,2)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,2)

# Word 3 Gram
ngram_dict = generateList(sarcastic_training_path,ngram_dict,3)
ngram_dict = generateList(non_sarcastic_training_path,ngram_dict,3)

# cutoff 15
cutoff(ngram_dict,3)

print len(ngram_dict.keys())



# POS tager ngram

# In[7]:

ngram_pos_dict = dict()

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,3)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,3)

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,4)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,4)

ngram_pos_dict = generateList(sarcastic_training_pos_path,ngram_pos_dict,5)
ngram_pos_dict = generateList(non_sarcastic_training_pos_path,ngram_pos_dict,5)

# cutoff 25
cutoff(ngram_pos_dict,5)
print len(ngram_pos_dict.keys())



# Classification task

# In[8]:

def readlines(fileName):
    with open(fileName,'r') as r_read:
        return r_read.readlines()


# In[9]:

#Read data - generate feature set

training_labels = []
testing_features = []
sarc = 0

tweetList = readlines(sarcastic_training_path)
postList = readlines(sarcastic_training_pos_path)

sarc = len(tweetList)
tweetList+=readlines(non_sarcastic_training_path)
postList+=readlines(non_sarcastic_training_pos_path)

training_labels = [1]*sarc+[0]*(len(tweetList)-sarc)

print len(tweetList)
print len(postList)


# In[10]:

#shuffel lists
combined = zip(tweetList,postList,training_labels)
random.shuffle(combined)
tweetList[:], postList[:], training_labels[:] = zip(*combined)


# Generate Training Feature set

# In[11]:

#1,2,3
#3,4,5
def generateFeature(line,postLine):
    ng = dict()
    np = dict()
    for j in range(1,4):
        gram_n = find_ngrams(str(line).strip().split(),j)
        for g in gram_n:
            ng['+'.join(g)] = ng.get('+'.join(g),0)+1
        
        pos_gram_n = find_ngrams(str(postLine).strip().split(),j+2)
        for p in pos_gram_n:
            np['+'.join(p)] = np.get('+'.join(p),0)+1
    
    return (ng,np)


# In[12]:

training_features = []

ln = len(puncs)+len(ngram_dict.keys())+len(ngram_pos_dict.keys())
# training_features = numpy.zeros((len(tweetList),ln),dtype=numpy.int)
training_features = sparse.lil_matrix((len(tweetList),ln))


# In[13]:

ngram_keys = dict()
ngram_pos_keys = dict()

index = 0
for key in ngram_dict.keys():
    ngram_keys[key] = index
    index+=1

index = 0
for key in ngram_pos_dict.keys():
    ngram_pos_keys[key] = index
    index+=1



# In[14]:

for i in range(0,len(tweetList)):
    (ng,np) = generateFeature(tweetList[i],postList[i])

    punc_features = getPuncFeatures(tweetList[i])
    for j in range(0,len(punc_features)):
        training_features[i,j] = punc_features[j]

    for key in ng:
        if key in ngram_dict:
            training_features[i,len(puncs)+ngram_keys[key]] = ng[key]
            
    for key in np:
        if key in ngram_pos_dict:
            training_features[i,len(puncs)+len(ngram_keys)+ngram_pos_keys[key]] = np[key]

training_features = normalize(training_features,axis=0)


# Run SVM

print "running SVM training"
# In[ ]:
clf = LinearSVC()
clf.fit(training_features, training_labels)  


# Get test data

# In[23]:

testing_features = []

test_tweetList = readlines(sarcastic_testing_path)
test_postList = readlines(sarcastic_testing_pos_path)
ct = len(test_tweetList)

test_tweetList+=readlines(non_sarcastic_testing_path)
test_postList+=readlines(non_sarcastic_testing_pos_path)

test_label = [1]*ct+[0]*(len(test_tweetList)-ct)


# In[24]:

testing_features = []

ln = len(puncs) + len(ngram_dict.keys())+len(ngram_pos_dict.keys())
testing_features = sparse.lil_matrix( (len(test_tweetList),ln))


# In[25]:

for i in range(0,len(test_tweetList)):
    (ng,np) = generateFeature(test_tweetList[i],test_postList[i])
    
    punc_features = getPuncFeatures(test_tweetList[i])
    for j in range(0,len(punc_features)):
        testing_features[i,j] = punc_features[j]

    for key in ng:
        if key in ngram_dict:
            testing_features[i,len(puncs) + ngram_keys[key]] = ng[key]
            
    for key in np:
        if key in ngram_pos_dict:
            testing_features[i,len(puncs) + len(ngram_keys)+ngram_pos_keys[key]] = np[key]


testing_features = normalize(testing_features,axis=0)


for c in [0.001,0.01,0.1,10,100,1000]:
    result_class = clf.predict(testing_features)
    print(metrics.classification_report(test_label, result_class, target_names=['non_sarcasm','sarcasm']))

    clf = LinearSVC(C=c)
    clf.fit(training_features, training_labels) 