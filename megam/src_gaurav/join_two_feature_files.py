import sys

folder = "/home/gaurav/WinDown/NLP/megam_files/traintest/"
train1 = open(folder + "training_feature_file_ngrams_pos_large_original.txt")
#train2 = open(folder + "training_feature_file_senti_initial_terminal.txt")
#trainR = open(folder + "training_feature_file_ngrams_pos_senti_initial_terminal_large.txt", 'w')
train2 = open(folder + "training_feature_file_parse_root_bigram.txt")
trainR = open(folder + "training_feature_file_ngrams_pos_parse_root_bigram_large.txt", 'w')
test1 = open(folder + "testing_feature_file_ngrams_pos_large_original.txt")
test2 = open(folder + "testing_feature_file_parse_root_bigram.txt")
testR = open(folder + "testing_feature_file_ngrams_parse_root_bigram_large.txt", 'w')
#test2 = open(folder + "testing_feature_file_senti_initial_terminal.txt")
#testR = open(folder + "testing_feature_file_ngrams_pos_senti_initial_terminal_large.txt", 'w')

line1 = train1.readline()
line2 = train2.readline()
while(line1 != ''):
    linef = line1.strip() + " "+ line2.strip()
    trainR.write(linef.rstrip()+"\n")
    line1 = train1.readline()
    line2 = train2.readline()

train1.close()
train2.close()
trainR.close()

line1 = test1.readline()
line2 = test2.readline()
while(line1 != ''):
    linef = line1.strip() + " "+ line2.strip()
    testR.write(linef.rstrip()+"\n")
    line1 = test1.readline()
    line2 = test2.readline()

test1.close()
test2.close()
testR.close()
