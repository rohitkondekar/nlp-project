import sys
import re

folder = "/home/gaurav/WinDown/NLP/megam_files/output/"
inputf  = open(folder + "ngrams_pos_senti_initial_terminal_binomial")
outputf = open(folder + "ngrams_pos_senti_initial_terminal_binomial_filtered_55.txt", 'w')

line = inputf.readline()
while(line != ''):
    lineOut = '0'
    line = re.split(r'\t+', line.strip())
    num = float(line[1])
    if num > 0.55:
        lineOut = '1'
    outputf.write(lineOut+'\n')
    line = inputf.readline()
inputf.close()
outputf.close()
