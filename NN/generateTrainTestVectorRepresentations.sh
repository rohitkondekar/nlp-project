echo "1"
python3.4 generateTrainingData.py word2vec/vectors/all_tokens_large_100_3.txt low_mode1_100_ ../../../tweets/final/original/ 1 &
echo "2"
python3.4 generateTrainingData.py word2vec/vectors/all_tokens_large_100_3.txt low_mode2_100_ ../../../tweets/final/original/ 2 &
echo "3"
python3.4 generateTrainingData.py word2vec/vectors/all_tokens_large_100_3.txt low_mode3_100_ ../../../tweets/final/original/ 3 &
echo "4"
python3.4 generateTrainingData.py word2vec/vectors/all_tokens_large_100_3.txt low_mode4_200_ ../../../tweets/final/original/ 4 &
echo "5"
python3.4 generateTrainingData.py word2vec/vectors/all_tokens_large_100_3.txt low_mode5_200_ ../../../tweets/final/original/ 5 &

echo "MID"
echo "1"
python3.4 generateTrainingData.py word2vec/vectors/mid_filter_large_100_3.txt mid_mode1_100_ ../../../tweets/final/mid/ 1 &
echo "2"
python3.4 generateTrainingData.py word2vec/vectors/mid_filter_large_100_3.txt mid_mode2_100_ ../../../tweets/final/mid/ 2 &
echo "3"
python3.4 generateTrainingData.py word2vec/vectors/mid_filter_large_100_3.txt mid_mode3_100_ ../../../tweets/final/mid/ 3 &
echo "4"
python3.4 generateTrainingData.py word2vec/vectors/mid_filter_large_100_3.txt mid_mode4_200_ ../../../tweets/final/mid/ 4 &
echo "5"
python3.4 generateTrainingData.py word2vec/vectors/mid_filter_large_100_3.txt mid_mode5_200_ ../../../tweets/final/mid/ 5 &

echo "FULL"
echo "1"
python3.4 generateTrainingData.py word2vec/vectors/full_filter_large_100_3.txt high_mode1_100_ ../../../tweets/final/full/ 1 &
echo "2"
python3.4 generateTrainingData.py word2vec/vectors/full_filter_large_100_3.txt high_mode2_100_ ../../../tweets/final/full/ 2 &
echo "3"
python3.4 generateTrainingData.py word2vec/vectors/full_filter_large_100_3.txt high_mode3_100_ ../../../tweets/final/full/ 3 &
echo "4"
python3.4 generateTrainingData.py word2vec/vectors/full_filter_large_100_3.txt high_mode4_200_ ../../../tweets/final/full/ 4 &
echo "5"
python3.4 generateTrainingData.py word2vec/vectors/full_filter_large_100_3.txt high_mode5_200_ ../../../tweets/final/full/ 5 &
