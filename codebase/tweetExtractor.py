from twython import Twython, TwythonError
import json
import time
import re

twitter = Twython("nGE6fmaRh6iGkfo5UqdgbKhrf", "Pe8lQPUGb6q3GMrPvlMw7Vqf49CCtfAbsxGKagYOxn9cgG8Mrp", "1192268754-1d3YUotpae8kGM8iXVj7Iw27Ug1LjfOk1ycJpzd", "51TR3qSWc74twkxDZSLMHz6MRe3RhdxvW8KfKB9SSJZ8a")

# pattern = re.compile(ur'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')

link = re.compile(r'http|www|https')
rt = re.compile(r'^RT')
MAX_ATTEMPTS = 10000000
# Max Number of tweets per 15 minutes
COUNT_OF_TWEETS_TO_BE_FETCHED = 10000000

for i in range(0,MAX_ATTEMPTS):
	if(COUNT_OF_TWEETS_TO_BE_FETCHED <= 0):
		break

	if(i==0):
		results = twitter.search(q="#sarcastic",count='100',lang='en',max_id='')
	else:
		results = twitter.search(q="#sarcastic",include_entities='true', lang='en', max_id=next_max_id)

	# print len(results['statuses'])
	for  result in results['statuses']:
		tweet =  result['text'].encode('utf-8').strip()

		#if tweet contains link, ignore tweet
		if link.search(tweet) is not None:
			continue
		if rt.search(tweet) is not None:
			continue

		#remove RT tag
		#tweet = re.sub(r'^RT',"",tweet)
		
		#Remove non-latin characters
		tweet = re.sub(ur'[^#\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', tweet)

		#Remove words starting with @
		tweet = re.sub('@[^ ]*','',tweet)
		
		#Remove words starting with #
		# tweet = re.sub('#[^ ]*','',tweet)
		
		#Remove Special Characters
		tweet = re.sub('[/\n:;]','',tweet)
		
		#lowercase everything
		tweet = tweet.lower()

		if len(tweet.split())<=2:
			continue

		print tweet


	COUNT_OF_TWEETS_TO_BE_FETCHED-=len(results)

	# Gets results every 5 seconds equaling 180 requests per 15 mins
	time.sleep(5)
	try:
		next_results_url_params = results['search_metadata']['next_results']
		next_max_id = next_results_url_params.split('max_id=')[1].split('&')[0]
	except:
		print "DONE."
		break





