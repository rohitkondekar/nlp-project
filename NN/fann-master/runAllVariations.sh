cd sarcasm_mixed
ephocs=1000
echo " --------------- Low --------------------"
input=100
mode="low_mode1_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="low_mode2_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="low_mode3_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
input=200
mode="low_mode4_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="low_mode5_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &

echo " --------------- Mid --------------------"
input=100
mode="mid_mode1_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="mid_mode2_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="mid_mode3_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
input=200
mode="mid_mode4_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="mid_mode5_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &

echo " --------------- High --------------------"
input=100
mode="high_mode1_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="high_mode2_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="high_mode3_100_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
input=200
mode="high_mode4_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &
mode="high_mode5_200_"
bash run.sh ../../trainTestData/$mode $mode $input $ephocs &

cd ..
exit
