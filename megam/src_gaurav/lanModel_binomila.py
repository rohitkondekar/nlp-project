import sys
import re

folder = "/home/gaurav/WinDown/NLP/megam_files/output/"
inputL  = open("/home/gaurav/WinDown/NLP/megam_files/traintest/result_file_lan_model.txt")
inputB = open(folder + "ngrams_pos_senti_initial_terminal")
output = open(folder + "Lan_Binomial.txt", 'w')

lineL = inputL.readline().strip()
lineB = inputB.readline().strip()
while(lineL != ''):
    lineL = int(lineL)
    lineB = int(lineB)
    lineOut = 0
    if lineL == 1 and lineB == 1:
        lineOut = 1
    output.write(str(lineOut)+'\n')
    lineL = inputL.readline().strip()
    lineB = inputB.readline().strip()
inputL.close()
inputB.close()
output.close()
