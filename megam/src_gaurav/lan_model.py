import sys
import json
import math
import re
import random
from copy import copy, deepcopy

def addToDict(word, dict_type):
    if word in dict_type:
        dict_type[word] += 1
    else:
        dict_type[word] = 1
    return dict_type

dict_sar = dict()
dict_non = dict()

folder = "data/"
sarcastic_training_path = folder + "train_25/final_1_tweets_sarcasm_train.txt"
non_sarcastic_training_path = folder + "train_25/final_1_tweets_non_sarcasm_tokenized_en_training.txt"

sarcastic_testing_path = folder+"test/final_1_tweets_sarcasm_test.txt"
non_sarcastic_testing_path = folder+"test/final_1_tweets_non_sarcasm_tokenized_en_testing.txt"

# Megam train and test data paths
w_file_path = '/home/gaurav/WinDown/NLP/megam_files/traintest/result_file_lan_model.txt'
wt_file_path = '/home/gaurav/WinDown/NLP/megam_files/traintest/test_file_lan_model.txt'

sar_file_train = open(sarcastic_training_path)
non_file_train = open(non_sarcastic_training_path)

line = sar_file_train.readline()
while line != '':
    words = line.strip().split()
    for word in words:
        dict_sar = addToDict(word, dict_sar)
    line = sar_file_train.readline()

line = non_file_train.readline()
while line != '':
    words = line.strip().split()
    for word in words:
        dict_non = addToDict(word, dict_non)
    line = non_file_train.readline()

count_sar_vocab = 0
count_non_vocab = 0

for w in dict_sar:
    count_sar_vocab += dict_sar[w]

for w in dict_non:
    count_non_vocab += dict_non[w]

count_sar_vocab += len(dict_sar)
count_non_vocab += len(dict_non)

# Generate Testing Data
sar_file_test = open(sarcastic_testing_path)
non_file_test = open(non_sarcastic_testing_path)
w_file = open(w_file_path, 'w')
wt_file = open(wt_file_path, 'w')

inputv = 1
line = sar_file_test.readline()
while line != '':
    words = line.strip().split()
    prob_sar = 0
    count_loop = 0
    prob_non = 0
    for word in words:
        if word in dict_sar:
            prob_sar += math.log(dict_sar[word]+1)
        else:
            prob_sar += math.log(1)
        if word in dict_non:
            prob_non += math.log(dict_non[word]+1)
        else:
            prob_non += math.log(1)
        count_loop += 1
    outputv = 0
    prob_sar = prob_sar - count_loop*math.log(count_sar_vocab)
    prob_non = prob_non - count_loop*math.log(count_non_vocab)
    if prob_sar >= prob_non:
        outputv = 1
    wt_file.write(str(inputv)+'\n')
    w_file.write(str(outputv)+'\n')
    line = sar_file_test.readline()

inputv = 0
line = non_file_test.readline()
while line != '':
    words = line.strip().split()
    prob_sar = 0
    prob_non = 0
    count_loop = 0
    for word in words:
        if word in dict_sar:
            prob_sar += math.log(dict_sar[word]+1)
        else:
            prob_sar += math.log(1)
        if word in dict_non:
            prob_non += math.log(dict_non[word]+1)
        else:
            prob_non += math.log(1)
        count_loop += 1
    outputv = 0
    prob_sar = prob_sar - count_loop*math.log(count_sar_vocab)
    prob_non = prob_non - count_loop*math.log(count_non_vocab)
    if prob_sar >= prob_non:
        outputv = 1
    wt_file.write(str(inputv)+'\n')
    w_file.write(str(outputv)+'\n')
    line = non_file_test.readline()

wt_file.close()
w_file.close()

